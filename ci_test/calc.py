def add(x: int, y: int) -> int:
    return x + y


def diff(x: int, y: int) -> int:
    return x - y


def mul(x: int, y: int) -> int:
    return x * y


def div(x: int, y: int) -> float:
    return x / y
