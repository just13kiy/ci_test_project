from celery import Celery
from config import config

app = Celery('celery')

app.config_from_object(config, namespace='CELERY')
app.autodiscover_tasks(['ci_test'])

if __name__ == '__main__':
    app.start()
