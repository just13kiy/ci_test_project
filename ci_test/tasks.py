from ci_test.calc import add, diff, div, mul
from .celery import app


@app.task()
def add_task(x: int, y: int) -> int:
    return add(x, y)


@app.task()
def diff_task(x: int, y: int) -> int:
    return diff(x, y)


@app.task()
def div_task(x: int, y: int) -> float:
    return div(x, y)


@app.task()
def mul_task(x: int, y: int) -> int:
    return mul(x, y)
