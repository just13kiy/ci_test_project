from ci_test.tasks import add_task, diff_task, div_task, mul_task


if __name__ == '__main__':
    tasks = [add_task, diff_task, div_task, mul_task]

    res = [task.apply_async((3, 4)) for task in tasks]
    res = list(map(lambda x: x.get(), res))
    print(res)
