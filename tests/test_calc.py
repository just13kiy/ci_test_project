import pytest
from ci_test import calc


@pytest.mark.unit
class TestCalc:
    @pytest.mark.parametrize('x, y, expected', [
        (1, 1, 2),
        (-1, 1, 0),
        (1, -1, 0),
        (-1, -1, -2),
    ])
    def test_add(self, x, y, expected):
        result = calc.add(x, y)
        assert expected == result

    @pytest.mark.parametrize('x, y, expected', [
        (1, 1, 0),
        (-1, 1, -2),
        (1, -1, 2),
        (-1, -1, 0),
    ])
    def test_diff(self, x, y, expected):
        result = calc.diff(x, y)
        assert expected == result

    @pytest.mark.parametrize('x, y, expected', [
        (1, 1, 1),
        (-1, 1, -1),
        (1, -1, -1),
        (-1, -1, 1),
    ])
    def test_mul(self, x, y, expected):
        result = calc.mul(x, y)
        assert expected == result

    @pytest.mark.parametrize('x, y, expected', [
        (1, 1, 1.0),
        (-1, 1, -1.0),
        (1, -1, -1.0),
        (-1, -1, 1.0),
    ])
    def test_div(self, x, y, expected):
        result = calc.div(x, y)
        assert expected == result
