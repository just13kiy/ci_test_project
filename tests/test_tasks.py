import pytest
from ci_test import tasks


@pytest.mark.celery_backend
class TestTasks:
    @pytest.mark.parametrize('x, y, expected', [
        (1, 1, 2),
        (-1, 1, 0),
        (1, -1, 0),
        (-1, -1, -2),
    ])
    def test_add_task(self, x, y, expected):
        result = tasks.add_task.apply_async((x, y))
        assert expected == result.get()

    @pytest.mark.parametrize('x, y, expected', [
        (1, 1, 0),
        (-1, 1, -2),
        (1, -1, 2),
        (-1, -1, 0),
    ])
    def test_diff(self, x, y, expected):
        result = tasks.diff_task.apply_async((x, y))
        assert expected == result.get()

    @pytest.mark.parametrize('x, y, expected', [
        (1, 1, 1),
        (-1, 1, -1),
        (1, -1, -1),
        (-1, -1, 1),
    ])
    def test_mul(self, x, y, expected):
        result = tasks.mul_task.apply_async((x, y))
        assert expected == result.get()

    @pytest.mark.parametrize('x, y, expected', [
        (1, 1, 1.0),
        (-1, 1, -1.0),
        (1, -1, -1.0),
        (-1, -1, 1.0),
    ])
    def test_div(self, x, y, expected):
        result = tasks.div_task.apply_async((x, y))
        assert expected == result.get()
